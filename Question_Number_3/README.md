3A
Tabel Category
![alt text](https://dl.dropbox.com/s/1l8aq9mggaw8faf/Category.jpeg)

Tabel Cashier
![alt text](https://dl.dropbox.com/s/gj7cqha7ny6yx1c/Cashier.jpeg)

Tabel Product
![alt text](https://dl.dropbox.com/s/ettltukzobu8ptl/Product.jpeg)

Query Syntax 
SELECT cashier.name AS "Cashier", product.name AS "Product", category.name AS "Category", product.price AS "Price"
FROM category, cashier, product
WHERE product.id_catergory = category.id AND product.id_cashier = cashier.id
![alt text](https://dl.dropbox.com/s/79d96g8bnat9z56/Query.jpeg)
